class Fixnum
  def in_words
    # only set up to go to trillions!
    raise 'object must be [0, 10**15)' unless self >= 0 && self < 10**15
    string = []
    return 'zero' if zero?
    triplets = split_to_triples
    len = triplets.length
    triplets.each_with_index do |a, i|
      next if a.all? { |e| e.zero? }

      string << single_digit(a[0]) + ' hundred' unless a[0].zero?

      if a[1] == 1
        string << ten_to_19(a[1..2].join.to_i)
      elsif a[1] != 0
        string << twenty_to_90(a[1])
      end

      string << single_digit(a[2]) unless a[1] == 1 || a[2].zero?
      string << magnitide(len - i - 1) unless len - 1 == i

    end
    string.join(' ')
  end

  def split_to_triples
    # returns nested array of 3 element arrays, 12345 -> [[0,1,2], [3,4,5]]
    arr = to_s.chars.map(&:to_i)
    len = arr.length
    rem = len % 3
    final = []
    if len < 4
      final << arr
    elsif rem.zero?
      final << arr.each_slice(3).to_a
    else
      final << arr.take(rem)
      final.concat(arr[rem..-1].each_slice(3).to_a)
    end
    final[0] = final[0].unshift(0) while final[0].length < 3
    final
  end

  def single_digit(n)
    ints = (1..9).to_a
    words = %w[one two three four five six seven eight nine]
    Hash[ints.zip(words)][n]
  end

  def ten_to_19(n)
    ints = (10..19).to_a
    words = %w[ten eleven twelve thirteen fourteen fifteen sixteen
              seventeen eighteen nineteen]
    Hash[ints.zip(words)][n]
  end

  def twenty_to_90(n)
    ints = (1..9).to_a
    words = %w[ten twenty thirty forty fifty sixty seventy eighty ninety]
    Hash[ints.zip(words)][n]
  end

  def magnitide(n)
    mags = %w[trillion billion million thousand] << ''
    indexes = (0 ... mags.length).to_a
    Hash[indexes.zip(mags.reverse)][n]
  end
end
